package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PropertyResponseDto {

	private double emi;
	private String message;
	private int statusCode;
}
