package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MortgageRequestDto {

	private String panCard;
	private String paymentType;
}
