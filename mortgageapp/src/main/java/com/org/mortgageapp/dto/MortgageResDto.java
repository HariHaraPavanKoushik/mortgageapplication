package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MortgageResDto {

	private int statusCode;
	private String message;
	private String customerId;
	private String savingsAccountNumber;
	private double savingsAccountBalance;
	private String mortgageAccountNumber;
	private double mortgageAccountBalance;
	private double emi;

}
