package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaymentDto {

	private String paymentType;
	private double emi;

}
