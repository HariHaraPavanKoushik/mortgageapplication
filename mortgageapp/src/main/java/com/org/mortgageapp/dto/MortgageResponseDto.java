package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MortgageResponseDto {

	private String customerId;
	private String password;
	private String savingsAccountNumber;
	private String mortgageAccountNumber;
	private double savingsAccountBalance;
	private double mortgageAccountBalance;
	private double emi;
	private String paymentType;
	private String message;
	private int statusCode;
}
