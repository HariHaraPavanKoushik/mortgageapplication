package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PropertyRequestDto {

	private String panCard;
	private double propertyValue;
	private String propertyType;
	private double initialDeposit;
	private int tenure;
}
