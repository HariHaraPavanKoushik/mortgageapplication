package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MortgageReqDto {
	
	private String customerId;

	private String password;
	
}