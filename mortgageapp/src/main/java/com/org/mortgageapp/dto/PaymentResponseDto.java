package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaymentResponseDto {

	private String statusMessage;
	private int statusCode;
	
	public PaymentResponseDto(String statusMessage, int statusCode) {
		super();
		this.statusMessage = statusMessage;
		this.statusCode = statusCode;
	}

	
}
