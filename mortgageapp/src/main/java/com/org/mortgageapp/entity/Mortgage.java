package com.org.mortgageapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Mortgage {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long mortgageId;
	
	private String customerId;
	private String password;
	private String savingsAccountNumber;
	private double savingsAccountBalance;
	private String mortgageAccountNumber;
	private double mortgageAccountBalance;
	private double emi;
	private String paymentType;
	
	 @OneToOne(fetch = FetchType.LAZY, optional = false)
	    @JoinColumn(name = "user_id", nullable = false)
	    private User user;
	
}
