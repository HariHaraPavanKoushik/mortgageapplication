package com.org.mortgageapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Property {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long propertyId;
	
	private String propertyType;
	private double propertyValue;
	private double initialDeposit;
	private int tenure;
	private double emi;
	
	  @OneToOne(fetch = FetchType.LAZY, optional = false)
	    @JoinColumn(name = "user_id", nullable = false)
	    private User user;
}
