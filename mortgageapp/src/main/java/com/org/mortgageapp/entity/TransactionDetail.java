package com.org.mortgageapp.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "TRANSACTION_DETAIL")
public class TransactionDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long transactionId;
	
	@Column(name = "CUSTOMER_ID")
	private String customerId;
	
	@Column(name = "EMI")
	private double emi;
	
	@Column(name = "DATE")
	private LocalDate date;
	
}
