package com.org.mortgageapp.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorStatus {

	int statuscode;
	String statusmessage;
}
